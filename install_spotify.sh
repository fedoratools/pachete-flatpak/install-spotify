#!/bin/bash

# Verificăm dacă scriptul este rulat cu permisiuni de root
if [ "$EUID" -ne 0 ]; then
  echo "Te rog rulează scriptul cu permisiuni de root."
  exit
fi

# Actualizăm sistemul
dnf update -y

# Instalăm Flatpak dacă nu este deja instalat
dnf install flatpak -y

# Adăugăm repository-ul Flathub, dacă nu este deja adăugat
if ! flatpak remote-list | grep -q flathub; then
  flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
fi

# Instalăm Spotify folosind Flatpak
flatpak install flathub com.spotify.Client -y

# Verificăm instalarea
if flatpak list | grep com.spotify.Client; then
  echo "Spotify a fost instalat cu succes!"
else
  echo "A apărut o problemă la instalarea Spotify."
fi

