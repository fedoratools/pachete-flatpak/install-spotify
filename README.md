# Script pentru instalarea Spotify folosind Flatpak

Acest script Bash automatizează procesul de instalare a Spotify folosind Flatpak pe sistemele care folosesc gestionarul de pachete DNF (Fedora, CentOS, RHEL etc.).

## Cerințe

- Sistem de operare care utilizează DNF (Fedora, CentOS, RHEL etc.)
- Acces de root pentru rularea scriptului

## Instrucțiuni de utilizare

1. Descărcați scriptul `install_spotify.sh`.
2. Deschideți terminalul și navigați la directorul în care ați descărcat scriptul.
3. Asigurați-vă că aveți permisiuni de root și rulați scriptul folosind comanda:

    ```bash
    sudo ./install_spotify.sh
    ```

## Descriere

- Verifică dacă scriptul este rulat cu permisiuni de root.
- Actualizează sistemul folosind `dnf update -y`.
- Instalează Flatpak dacă nu este deja instalat.
- Adaugă repository-ul Flathub dacă nu este deja adăugat.
- Instalează Spotify folosind Flatpak.
- Verifică instalarea și afișează un mesaj corespunzător.

## Notă

Pentru a rula scriptul, este necesară conexiune la internet pentru a descărca și instala Spotify folosind Flatpak.